package unknown;

import static org.junit.Assert.assertSame;

import org.junit.Before;
import org.junit.Test;

import datastructures.Stack;

public class StackTest {

	private Stack<String> element;

	@Before
	public void setUp() {
		element = new Stack<String>();
		element.push("First");
	}

	@Test
	public void getShouldReturnPutObject() {
		String s = "Second";
		element.push(s);
		assertSame(s, element.pop());
	}

	@Test
	public void testMultiple() {
		String second = "Second";
		element.push(second);
		String third = "Third";
		element.push(third);
		assertSame("Objects aren�t returned in LIFO order", third, element.pop());
		assertSame("Objects aren�t returned in LIFO order", second, element.pop());
	}

}
