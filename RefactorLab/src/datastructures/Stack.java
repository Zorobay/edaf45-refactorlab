package datastructures;

public class Stack <E>{

    java.util.LinkedList<E> list = new java.util.LinkedList<E>();
    
    public void push(E e) {
        list.add(e);
    }
    
    public E pop() {
        return list.removeLast();
    }

}
